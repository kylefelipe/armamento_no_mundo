# Armamento no Mundo

Há uma discussão muito séria no [Brasil](https://pt.wikipedia.org/wiki/Brasil) a respeito das leis de armamentos, onde há um apelo muito grande para a facilitação da população de ter acesso a armamento, com a premissa de que o cidadão de bem deveria ter o direito de se defender, ja outros não entendem que isso seria uma boa idéia.
E para dar embasamento a seu ponto de vista essa pessoas term difundido nas redes sociais muitos dados, e imagens, de lideres mundiais, seu ponto de vista a resepeito do assunto e sua posição politica.
E algumas coisas me incomodaram nessa discussão toda:
*  Primeiro - É o víciamento dos fatos apresentados para embasamento da forma de pensar de cada um, sem levar em consideração o contexto político, histórico da época.
*  Segundo - Era sempre os mesmos extremos apresentados, normalmente aqueles considerados "bons lideres" apoiando o armamento e os "maus lideres" apoiando o desarmamento do povo.
*  Terceiro - A falta de informação a respeito de criminalidade, nivel de escolaridade, econômico dos paises em ambos os lados.  

Esse assunto já vem martelando no meu pensamento há muito tempo e gostaria de aprofundar nele para chegar a uma opinião bem embasada.  

A principio, o projeto vai ser feito em 2 momentos:  

Momento 1 - Dados Globais.  

Momento 2 - Dados Nacionais.  

No Momento 1, a intenção é buscar os dados dos países a respeito de sua legislação, seus lideres e suas posições, bem como o contexto histórico, dados sobre economia, educação, as taxas de criminalidade e etc...

No Momento 2, a intenção é fazer isso tudo, só que apenas para o Brasil.

Para poder conseguir juntar isso tudo em um só lugar, pretendo utilizar de ferramentas como:
 - Qgis - analise e tratamento dos dados
 - Qgis Server - Publicação de mapas.
 - PostgreSQL/Postgis - Banco de dados para receber tais dados.
 - Python - Para agilizar mais as coisas.
 Todas essas coisas para gerarem um webgis bem legal, mapas interativos e muito mais.